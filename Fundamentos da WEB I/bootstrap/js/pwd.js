function gerar(){
  var chars = ["A","B","C","D","E","F","G","H","Y","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","W","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","x","w","z","0","1","2","3","4","5","6","7","8","9"];
  var pwd = "";
  var posicao = 0;
    for (var i = 0; i < 10; i++) {
      posicao = parseInt(Math.random()*60);
      pwd += chars[posicao];
    }
    document.getElementById('senha').value = pwd;
    verificar();
}

function verificar(){
  var senha = document.getElementById("senha").value;
  if (senha.length == 0) {
    document.getElementById("forca").innerHTML = "";
  } else if (senha.length <= 5){
    document.getElementById("forca").innerHTML = "Fraca";
  } else if (senha.length >= 10){
    document.getElementById("forca").innerHTML = "Forte";
  } else {
    document.getElementById("forca").innerHTML = "Média";
  }
}

function listar(){

  $.getJSON("assets/testejson.php", function(contatos){

    for(var i in contatos){
      //CRIAR UM ITEM DA LISTA
      var li = document.createElement("li");
      //INJETAR TEXTO ENTRE AS TAGS DO ITEM DA LISTA
      li.innerHTML = contatos[i].nome.toUpperCase() + " (" + contatos[i].cpf + ")";
      document.getElementById("lista").appendChild(li);
    }
  });
}

function salvar(formulario){

    var li = document.createElement("li");
    li.innerHTML = formulario.nome.value.toUpperCase() + " (" + formulario.idade.value + " anos)";

    formulario.nome.value = "";
    formulario.idade.value = "";

    document.getElementById("lista").appendChild(li);
  return false;
}
